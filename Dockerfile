FROM archlinux:base-devel
ADD 'https://gitlab.com/InsanePrawn/deviceinfo-stub/-/jobs/1228986543/artifacts/raw/deviceinfo-fake-fake.0-1-x86_64.pkg.tar.zst?inline=false' /var/cache/pacman/pkg/
RUN pacman -Sy
RUN pacman -U /var/cache/pacman/pkg/deviceinfo-fake-fake.0-1-x86_64.pkg.tar.zst --noconfirm
