#!/bin/bash
set -eu
docker pull archlinux:base-devel
docker build -t archlinux:base-devel-phonemod .
docker tag archlinux:base-devel-phonemod archlinux:base-devel
